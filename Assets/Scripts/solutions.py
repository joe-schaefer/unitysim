from numpy import arange,zeros_like,array
import scipy.integrate as integrate

from argparse import ArgumentParser

from math import pi, trunc
from numpy import sin, cos

def log(string):
        with open('uniqly_named_log_file.txt','a') as f:
            f.write(string+'\n')
            f.close()

log("15")
parser = ArgumentParser()
parser.add_argument('invert',type=int)
parser.add_argument('g',type=float)
parser.add_argument('l',type=float)
parser.add_argument('m',type=float)
parser.add_argument('M',type=float)
parser.add_argument('theta',type=float)
parser.add_argument('theta_dot',type= float)
parser.add_argument('x',type=float)
parser.add_argument('x_dot',type=float)
parser.add_argument('x_0',type=float)
parser.add_argument('tMax',type=int)
args = parser.parse_args()
log("29")
log(str(args))

g=args.g 
l = args.l
m = args.m
M = args.M
theta = args.theta
theta_dot = args.theta_dot
x = args.x
x_dot = args.x_dot
x_0 = args.x_0
tMax= args.tMax
log("44")


if args.invert == 1:
    log("Inverted!")
    def trim(x, step):
        d = trunc(x / step)
        return step * d

    dt = 0.05

    # simulation time
    t = arange(0.0, tMax, dt)
    

    precision = 0.006

    Kp_theta = 50
    Kd_theta = 15
    Kp_x = 3.1
    Kd_x = 4.8

    state = array([theta, theta_dot, x, x_dot, trim(theta, precision), .0])

    def derivativesA(state, t):
        ds = zeros_like(state)

        _theta = state[0]
        _theta_dot = state[1]
        _x = state[2]
        _x_dot = state[3]

        u = Kp_theta * _theta + Kd_theta * _theta_dot + Kp_x * (_x - x_0) + Kd_x * _x_dot

        ds[0] = state[1]
        ds[1] = (g * sin(_theta) - u * cos(_theta)) / l
        ds[2] = state[3]
        ds[3] = u

        return ds

    solution = integrate.odeint(derivativesA, state, t)
    for a in solution:
        print(','.join([str(i) for i in a]))

    log(str(solution.shape))


elif args.invert == 0:
    log("Floppy!")
    b = m / (m + M)
    dt = 0.05
    # simulation time
    t = arange(0.0, tMax, dt)

    state = array([theta, theta_dot, x, x_dot])

    def derivativesB(state, t):
        ds = zeros_like(state)

        ds[0] = state[1]
        ds[1] = (g * sin(state[0]) + b * l * state[1]**2 * sin(state[0]) * cos(state[0])) / (l * (1 + b * cos(state[0])**2))
        ds[2] = state[3]
        ds[3] = b * (l * state[1]**2 * sin(state[0]) - g * sin(state[0]) * cos(state[0])) / (1 + b * cos(state[0])**2)

        return ds


    solution = integrate.odeint(derivativesB, state, t)
    
    for a in solution:
        print(','.join([str(i) for i in a]))


